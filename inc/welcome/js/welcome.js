jQuery(document).ready(function() {

  var counter = jQuery('#counter-count').data('counter');
  if ( counter != '0')  {  
    jQuery('li.newspapers-w-red-tab a').append('<span class="newspapers-actions-count">' + counter + '</span>');
  } else {
    jQuery('.newspapers-tab').removeClass( 'newspapers-w-red-tab' );
  }
	/* Tabs in welcome page */
	function newspapers_welcome_page_tabs(event) {
		jQuery(event).parent().addClass("active");
        jQuery(event).parent().siblings().removeClass("active");
        var tab = jQuery(event).attr("href");
        jQuery(".newspapers-tab-pane").not(tab).css("display", "none");
        jQuery(tab).fadeIn();
	}

	var newspapers_actions_anchor = location.hash;

	if( (typeof newspapers_actions_anchor !== 'undefined') && (newspapers_actions_anchor != '') ) {
		newspapers_welcome_page_tabs('a[href="'+ newspapers_actions_anchor +'"]');
	}

    jQuery(".newspapers-nav-tabs a").click(function(event) {
        event.preventDefault();
		newspapers_welcome_page_tabs(this);
    });

 /* Tab Content height matches admin menu height for scrolling purpouses */
		$tab = jQuery('.newspapers-tab-content > div');
		$admin_menu_height = jQuery('#adminmenu').height();
    if( (typeof $tab !== 'undefined') && (typeof $admin_menu_height !== 'undefined') )
  {
		$newheight = $admin_menu_height - 180;
		$tab.css('min-height',$newheight);
  }
});
