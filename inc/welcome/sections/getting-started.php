<?php
/**
 * Getting started template
 */

?>
<?php $theme = wp_get_theme(); ?>

<div id="getting_started" class="newspapers-tab-pane active">

	<div class="newspapers-tab-pane-center">

		<h1 class="newspapers-welcome-title"><?php printf( esc_html__( 'Welcome to %s!', 'newspapers' ), $theme->get( 'Name' ) ); ?></h1>
	<p><?php  esc_html__( 'We want to make sure you have the best experience using newspapers and that is why we gathered here all the necessary informations for you. We hope you will enjoy using newspapers, as much as we enjoy creating great products.', 'newspapers' ) ; ?>

	</div>

	<hr />



	<div class="newspapers-tab-pane-center">

		<h1><?php esc_html_e( 'FAQ', 'newspapers' ); ?></h1>

	</div>
  <div class="newspapers-video-tutorial">
    <div class="newspapers-tab-pane-half newspapers-tab-pane-first-half">
  		<h2><?php esc_html_e( 'Set a Static Home Page (Front Page)', 'newspapers' ); ?></h4>
      <p><?php esc_html_e( 'By default the homepage will look like a blog (this is how WordPress is intended to work). If you want a custom homepage like newspapers demo . then go to customize >Static Front Page and you can define your homepage here.
	On this video you will learn how you setup your website’s front page you like one for newspapers theme demo which you can see here.', 'newspapers' ); ?>
	<ul>

	<li>
	  <?php esc_html_e( 'Step 1 : Go to customize', 'newspapers' ); ?>
	</li>
	<li>
	  <?php esc_html_e( 'Step 2 : Static Front Page => A static page', 'newspapers' ); ?>
	</li>
	<li>
	  <?php esc_html_e( 'Step 3 : Post page => click add new page => blog', 'newspapers' ); ?>
	</li>
	<li>
	  <?php esc_html_e( 'Step 4 : Front page => click add new page => Home', 'newspapers' ); ?>
	</li>
	<li>
	  <?php esc_html_e( 'Step 5 : Save and published', 'newspapers' ); ?>
	</li>
	</ul>  <?php esc_html_e( 'That’s All', 'newspapers' ); ?></p>
  	  <p><a href="<?php echo esc_url( 'http://themezwp.com/newspapers-demo/documentation-usage/' ); ?>" class="button"><?php esc_html_e( 'View how to do this', 'newspapers' ); ?></a></p>
    </div>
    <div class="newspapers-tab-pane-half video">
      <p class="youtube">

    			<img src="<?php echo get_template_directory_uri(); ?>/inc/welcome/img/frontpage-min_iehsvn.jpg"  />

  		</p>
    </div>
  </div>



	<div class="newspapers-clear"></div>

</div>
