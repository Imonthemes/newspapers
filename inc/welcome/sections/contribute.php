<?php
/**
 * Contribute
 */
?>

<div id="contribute" class="newspapers-tab-pane">

	<h1><?php esc_html_e( 'How can I contribute?', 'newspapers' ); ?></h1>

	<hr/>

	<div class="newspapers-tab-pane-half newspapers-tab-pane-first-half">

		<p><strong><?php esc_html_e( 'Found a bug? Want to contribute with a fix?','newspapers'); ?></strong></p>

		<p><?php esc_html_e( 'Contact us!','newspapers' ); ?></p>

		<p>
			<a href="<?php echo esc_url( 'http://themes4wp.com/contact/' ); ?>" class="contribute-button button button-primary"><?php printf( esc_html__( '%s contact page', 'newspapers' ), 'newspapers' ); ?></a>
		</p>

		<hr>

	</div>
	<div class="newspapers-tab-pane-half">

		<p><strong><?php printf( esc_html__( 'Are you a polyglot? Want to translate %s into your own language?', 'newspapers' ), 'newspapers' ); ?></strong></p>

		<p><?php esc_html_e( 'Get involved at WordPress.org.', 'newspapers' ); ?></p>

		<p>
			<a href="<?php echo esc_url( 'https://translate.wordpress.org/projects/wp-themes/newspapers/' ); ?>" class="translate-button button button-primary"><?php printf( esc_html__( 'Translate %s', 'newspapers' ), 'newspapers' ); ?></a>
		</p>

	</div>

	<div>

		<h4><?php printf( esc_html__( 'Are you enjoying %s?', 'newspapers' ), 'newspapers' ); ?></h4>

		<p class="review-link"><?php printf( esc_html__( 'Rate our theme on %s. We\'d really appreciate it!', 'newspapers' ), '<a href="https://wordpress.org/support/view/theme-reviews/newspapers?filter=5">' . esc_html( 'WordPress.org', 'newspapers' ) . '</a>' ); ?></p>

		<p><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span></p>

	</div>

</div>
