<?php

/**
 * Add panels
 */


/* adding lanewspaperst panel */

Kirki::add_panel( 'upgradepro_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'About Theme', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all info about the Theme.', 'newspapers' ),

) );

Kirki::add_panel( 'bglayout_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'General settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all Site layout and Background color typography options of the Theme.', 'newspapers' ),

) );

Kirki::add_panel( 'header_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Header settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the header options of the Theme.', 'newspapers' ),
) );

Kirki::add_panel( 'homepage_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Home page settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the Home Page options of the Theme.', 'newspapers' ),
) );

Kirki::add_panel( 'newspaperspost_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Post settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the Post options of the Theme.', 'newspapers' ),
) );

Kirki::add_panel( 'newspaperspage_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Page settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the Page options of the Theme.', 'newspapers' ),
) );
if ( newspapers_is_woocommerce_active()) :
Kirki::add_panel( 'woocommercecustom_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Woocommerce settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the woocommerce options of the Theme.', 'newspapers' ),
) );
endif;

Kirki::add_panel( 'newspapersfooter_options', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Footer settings', 'newspapers' ),
    'description' => esc_attr__( 'This panel will provide all the footer options of the Theme.', 'newspapers' ),
) );
