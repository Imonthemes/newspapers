<?php
Kirki::add_config('newspapers', array(
    'capability' => 'edit_theme_options',
    'option_type' => 'theme_mod'
));

//**** newspapers upsell pro */
Kirki::add_field( 'newspapers', array(
	'type'        => 'custom',
	'settings'    => 'newspapers_view_link_pro',
	'section'     => 'newspapers_upgradepro_options',
	'default'     => '<a class="button-error  button-upsell" target="_blank" href="' . esc_url( 'https://themezwp.com/newspapers-pro/' ) . '">'.esc_html__( 'Upgrade To Pro', 'newspapers' ).'</a>',
	'priority'    => 10,
) );

Kirki::add_field( 'newspapers', array(
	'type'        => 'custom',
	'settings'    => 'newspapers_view_link1',
	'section'     => 'newspapers_upgradepro_options',
	'default'     => '<a class="button-success button-upsell" target="_blank" href="' . esc_url( 'https://themezwp.com/newspapers-demo/compare-free-vs-pro-ultimate/' ) . '">'.esc_html__( 'Pro Vs Free', 'newspapers' ).'</a>',
	'priority'    => 20,
) );

Kirki::add_field( 'newspapers', array(
	'type'        => 'custom',
	'settings'    => 'newspapers_view_link2',
	'section'     => 'newspapers_upgradepro_options',
	'default'     => '<a class="button-blue  button-upsell" target="_blank" href="' . esc_url( 'https://themezwp.com/forums/' ) . '">'.esc_html__( 'Support', 'newspapers' ).'</a>',
	'priority'    => 30,
) );


Kirki::add_field( 'newspapers', array(
	'type'        => 'custom',
	'settings'    => 'newspapers_view_link3',
	'section'     => 'newspapers_upgradepro_options',
	'default'     => '<a class="button-warning  button-upsell" target="_blank" href="' . esc_url( 'https://themezwp.com/newspapers-demo/documentation-usage/' ) . '">'.esc_html__( 'Read the documentation', 'newspapers' ).'</a>',
	'priority'    => 50,
) );


/* Top header */
Kirki::add_field('newspapers', array(
    'type' => 'toggle',
    'settings' => 'disable_top_header',
    'label' => esc_attr__('Disable Top Header', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'default' => '1',
    'priority' => 10
));


Kirki::add_field('newspapers', array(
    'type' => 'multicolor',
    'settings' => 'newspapers_topheader_color',
    'label' => esc_attr__('Top Bar color options ', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'text' => esc_attr__('Text ', 'newspapers'),
        'border_color' => esc_attr__('Boder ', 'newspapers'),
        'bgcolor' => esc_attr__('Background ', 'newspapers')
    ),
    'default' => array(
        'text' => '#282828',
        'bgcolor' => '#fff',
        'border_color' => '#ecede7'

    ),
    'output' => array(
        array(
            'choice' => 'text',
            'element' => '#topmenu .top-bar  .menu a',
            'property' => 'color'
        ),
        array(
            'choice' => 'bgcolor',
            'element' => '#topmenu',
            'property' => 'background-color'
        ),
        array(
            'choice' => 'border_color',
            'element' => '#topmenu',
            'property' => 'border-bottom-color'
        )
    )
));


Kirki::add_field('newspapers', array(
    'type' => 'dimension',
    'settings' => 'newspapers_topheadertext_size',
    'label' => esc_attr__('Top Bar menu text size', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'help' => esc_attr__('example: 10px, 3em,0.75rem, 48%, 90vh etc.', 'newspapers'),
    'default' => '0.75rem',
    'priority' => 10,
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#topmenu .top-bar .top-bar-left .menu a ',

            'property' => 'font-size',
            'units' => ''
        )

    )
));


Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_top_social',
    'section' => 'newspapers_topheader_settings',
    'priority' => 10,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Social button', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'open_social_tab',
    'label' => esc_attr__('Open in new tab', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'default' => '0',
    'priority' => 10
));


Kirki::add_field('newspapers', array(
    'type' => 'repeater',
    'label' => esc_attr__('Add social icon', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'priority' => 10,
    'row_label' => array(
        'type' => 'field',
        'value' => esc_attr__('Social', 'newspapers'),
        'field' => 'social_icon'
    ),
    'settings' => 'social_icons_top',
    'fields' => array(
        'social_icon' => array(
            'type' => 'select',
            'label' => esc_attr__('Icon', 'newspapers'),
            'default' => '',
            'choices' => array(
                '' => 'Please Select',
                'facebook' => esc_attr__('Facebook', 'newspapers'),
                'dribbble' => esc_attr__('Dribbble', 'newspapers'),
                'twitter' => esc_attr__('Twitter', 'newspapers'),
                'google' => esc_attr__('google plus', 'newspapers'),
                'skype' => esc_attr__('skype', 'newspapers'),
                'youtube' => esc_attr__('Youtube', 'newspapers'),
                'flickr' => esc_attr__('Flickr', 'newspapers'),
                'pinterest' => esc_attr__('Pinterest', 'newspapers'),
                'vk' => esc_attr__('vk', 'newspapers'),
                'rss' => esc_attr__('RSS', 'newspapers'),
                'tumblr' => esc_attr__('Tumblr', 'newspapers'),
                'instagram' => esc_attr__('Instagram', 'newspapers'),
                'xing' => esc_attr__('Xing', 'newspapers')
            )
        ),
        'social_url' => array(
            'type' => 'text',
            'label' => esc_attr__('Link URL', 'newspapers'),
            'default' => ''
        )
    )
));
Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'open_social_tab',
    'label' => esc_attr__('Open in new tab', 'newspapers'),
    'section' => 'newspapers_topheader_settings',
    'default' => '0',
    'priority' => 10
));

/* header & Logo */
Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_siteidentity',
    'section' => 'newspapers_headtitle_settings',
    'priority' => -1,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Header Layout', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'radio-image',
    'settings' => 'logo_position',
    'label' => esc_html__('Logo Position', 'newspapers'),
    'section' => 'newspapers_headtitle_settings',
    'default' => 'left',
    'priority' => 10,
    'choices' => array(
        'left' => get_template_directory_uri() . '/images/logo-left.png',
        'center' => get_template_directory_uri() . '/images/logo-center.png',
        'right' => get_template_directory_uri() . '/images/logo-right.png'
    ),
));


Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_headstyle',
    'section' => 'newspapers_headtitle_settings',
    'priority' => 100,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Header Styling', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_headerbg_color',
    'label' => esc_attr__(' Header background color', 'newspapers'),
    'section' => 'newspapers_headtitle_settings',
    'default' => '#ffffff',
    'priority' => 100,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),

    'output' => array(
        array(
            'element' => '#header-top .head-top-area,.mobile-menu .title-bar',
            'property' => 'background-color',
            'units' => ''
        )
    )

));


/*  title typography */

Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'title_typography',
    'label' => esc_attr__('Site title Typography', 'newspapers'),
    'section' => 'newspapers_headtitle_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto',
        'variant' => 'regular',
        'font-size' => '48px',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'color' => '#716e6e',
        'text-transform' => 'none'

    ),
    'priority' => 100,
    'output' => array(
        array(
            'element' => '#site-title .site-title,.site-title a'
        )
    )
));


Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'sitedescription_typography',
    'label' => esc_attr__('Site description Typography', 'newspapers'),
    'section' => 'newspapers_headtitle_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto',
        'variant' => 'regular',
        'font-size' => '12px',
        'line-height' => '0',
        'letter-spacing' => '2px',
        'subsets' => array(
            'latin-ext'
        ),
        'color' => '#716e6e',
        'text-transform' => 'none'
    ),
    'priority' => 100,
    'output' => array(
        array(
            'element' => '.title-bar-title  .site-description,#site-title .site-description'
        )
    )

));

/*----------- Main Menu -----------*/

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'disable_sticky_menu',
    'label' => esc_attr__('Disable sticky menu', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'newspapers_mainmenu_position',
    'label' => esc_attr__('Change alignment menu', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => 'top-bar-left',
    'priority' => 10,
    'multiple' => 1,
    'transport' => 'postMessage',
    'choices' => array(
        'top-bar-left' => esc_attr__('Left', 'newspapers'),
        'float-center' => esc_attr__('Center', 'newspapers'),
        'top-bar-right' => esc_attr__('Right', 'newspapers')
    )
));

$newspapers_head2_menubg = get_theme_mod('newspapers_head2_menubg', '#767676');
Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_head2_menubg',
    'label' => esc_attr__(' menu background color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#767676',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.head-bottom-area ',

            'property' => 'background-color',
            'units' => ''
        ),
        array(
            'element' => '.head-bottom-area.is-stuck',
            'property' => 'box-shadow',
            'value_pattern' => '0 2px 2px 0 ' . Kirki_Color::get_rgba($newspapers_head2_menubg, 14) . ', 0 3px 1px -2px ' . Kirki_Color::get_rgba($newspapers_head2_menubg, .2) . ', 0 1px 5px 0 ' . Kirki_Color::get_rgba($newspapers_head2_menubg, .12) . ''
        )


    )


));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_hover_texthover',
    'label' => esc_attr__('menu  Hover  color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#00bcd4',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .head-bottom-area .dropdown.menu a:hover',

            'property' => 'color',
            'units' => ''
        )
    )

));


Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_head2_stmenutext',
    'label' => esc_attr__(' menu  text color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#ffffff',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.head-bottom-area .dropdown.menu a,.search-wrap .search-field',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '.search-wrap::before',
            'property' => 'background-color',
            'units' => ''
        ),
        array(
            'element' => '.search-wrap',
            'property' => 'border-color',
            'units' => ''
        ),
        array(
            'element' => '.main-menu .is-dropdown-submenu .is-dropdown-submenu-parent.opens-left > a::after',
            'property' => 'border-right-color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_head2_submenubg',
    'label' => esc_attr__('Sub menu background  color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => 'rgba(33,33,33,0.9)',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.head-bottom-area .dropdown.menu .is-dropdown-submenu > li',

            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_head2_submenutext',
    'label' => esc_attr__('Sub menu text  color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#ffffff',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .head-bottom-area .dropdown.menu .is-dropdown-submenu > li a',

            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => ' .is-dropdown-submenu .is-dropdown-submenu-parent.opens-right > a::after',

            'property' => 'border-left-color',
            'units' => '!important'
        )

    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_hover_submenubg',
    'label' => esc_attr__('Sub menu background Hover  color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#00bcd4',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .head-bottom-area .desktop-menu .is-dropdown-submenu-parent .is-dropdown-submenu li a:hover',

            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_hover_submenutext',
    'label' => esc_attr__('Sub menu text Hover  color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#ffffff',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .head-bottom-area  .is-dropdown-submenu .is-dropdown-submenu-item :hover',

            'property' => 'color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_current_submenutext',
    'label' => esc_attr__('current menu text color', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'default' => '#00bcd4',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .head-bottom-area .dropdown.menu .current-menu-item a',

            'property' => 'color',
            'units' => ''
        )
    )

));
/*  menu typography */
Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'menus_typography',
    'label' => esc_attr__(' menu Typography', 'newspapers'),
    'section' => 'newspapers_mainmenu_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Roboto',
        'variant' => 'Bold 700',
        'font-size' => '14px',
        'letter-spacing' => '0.5px',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'uppercase '
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => '.head-bottom-area .dropdown.menu a'
        )
    )
));

/* * * * * * * Background and site layout * * * * * * */

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'secondary_bgcolor',
    'label' => esc_attr__('Front background color', 'newspapers'),
    'section' => 'newspapers_bglayout_settings',
    'default' => '#fff',
    'priority' => -1,
    'choices' => array(
        'alpha' => true
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => ' #main-content-sticky',
            'property' => 'background',
            'units' => ''
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'toggle',
    'settings' => 'newspapers_body_fullwidth',
    'label' => esc_attr__('Make website fullwidth', 'newspapers'),
    'section' => 'newspapers_mainlayout_settings',
    'default' => '0',
    'priority' => 10
));

Kirki::add_field('newspapers', array(
    'type' => 'dimension',
    'settings' => 'newspapers_body_topgap',
    'label' => esc_attr__('Site Top margin', 'newspapers'),
    'tooltip' => esc_attr__('example: 10px, 3em, 48%, 90vh etc.', 'newspapers'),
    'section' => 'newspapers_mainlayout_settings',
    'default' => '0px',
    'priority' => 10,
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#wrapper',
            'property' => 'margin-top',
            'units' => '',
            'media_query' => '@media screen and (min-width: 64em)'
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'dimension',
    'settings' => 'newspapers_body_bottomgap',
    'label' => esc_attr__('Site bottom margin', 'newspapers'),
    'section' => 'newspapers_mainlayout_settings',
    'default' => '0px',
    'transport' => 'auto',
    'tooltip' => esc_attr__('example: 10px, 3em, 48%, 90vh etc.', 'newspapers'),
    'priority' => 10,
    'output' => array(
        array(
            'element' => '#wrapper',
            'property' => 'margin-bottom',
            'units' => '',
            'media_query' => '@media screen and (min-width: 64em)'
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'typography_h1_setting',
    'label' => esc_attr__('H1 Typography ', 'newspapers'),
    'section' => 'newspapers_maintypography_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Lato',
        'variant' => '700',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'none'
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => 'h1'
        )
    )
));
Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'typography_h2_setting',
    'label' => esc_attr__('H2 Typography ', 'newspapers'),
    'section' => 'newspapers_maintypography_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Lato',
        'variant' => 'Regular',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'none'
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => 'h2'
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'typography_h3_setting',
    'label' => esc_attr__('H3 Typography and blog post widgets ', 'newspapers'),
    'tooltip' => esc_attr__('also work for newspapers custom blog widgets ', 'newspapers'),
    'section' => 'newspapers_maintypography_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Lato',
        'variant' => 'Regular',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'none'
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => 'h3'
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'typography_h4_setting',
    'label' => esc_attr__('H4 Typography ', 'newspapers'),
    'section' => 'newspapers_maintypography_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Lato',
        'variant' => 'Regular',
        'line-height' => '1.5',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'none'
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => 'h4'
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'typography',
    'settings' => 'typography_Paragraphs_setting',
    'label' => esc_attr__('Paragraphs Typography ', 'newspapers'),
    'section' => 'newspapers_maintypography_settings',
    'transport' => 'auto',
    'default' => array(
        'font-family' => 'Lato',
        'variant' => 'Regular',
        'font-size' => 'inherit',
        'line-height' => '1.6',
        'letter-spacing' => '0',
        'subsets' => array(
            'latin-ext'
        ),
        'text-transform' => 'none'
    ),
    'priority' => 10,
    'output' => array(
        array(
            'element' => 'p'
        )
    )
));


/* * * * * * * Home Page options * * * * * * */

/***** SLIDER setting ******/
Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_slider_enabel',
    'label' => esc_attr__('Enable/disabel Static image', 'newspapers'),
    'section' => 'slider_setup',
    'default' => '1',
    'priority' => 1,
    'choices' => array(
        'off' => esc_attr__('off', 'newspapers'),
        'on' => esc_attr__('on', 'newspapers')
    )
));


/* Slider */

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'category_show',
    'label' => esc_attr__('Select Category', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'priority' => 10,
    'multiple' => 999,
    'choices' => Kirki_Helper::get_terms('category')

));



Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'post_order_by',
    'label' => esc_attr__('Show post orderby', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'default' => 'date',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__('None', 'newspapers'),
        'date' => esc_attr__('Date', 'newspapers'),
        'ID' => esc_attr__('ID', 'newspapers'),
        'author' => esc_attr__('Author', 'newspapers'),
        'title' => esc_attr__('Title', 'newspapers'),
        'rand' => esc_attr__('Random', 'newspapers')
    )
));



Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_top_post',
    'section' => 'newspapers_homepage_slidersettings',
    'priority' => 10,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Top Post (appear in slider right side)', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'category_toppost_show',
    'label' => esc_attr__('Select Category', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'priority' => 10,
    'multiple' => 999,
    'choices' => Kirki_Helper::get_terms('category'),
));



Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'top_post_order_by',
    'label' => esc_attr__('Show post orderby', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'default' => 'date',
    'priority' => 10,
    'multiple' => 1,
    'choices' => array(
        'none' => esc_attr__('None', 'newspapers'),
        'date' => esc_attr__('Date', 'newspapers'),
        'ID' => esc_attr__('ID', 'newspapers'),
        'author' => esc_attr__('Author', 'newspapers'),
        'title' => esc_attr__('Title', 'newspapers'),
        'rand' => esc_attr__('Random', 'newspapers')
    ),

));

Kirki::add_field( 'newspapers', array(
	'type'        => 'color',
	'settings'    => 'slide_title_bgcolor',
	'label'       => __( 'Slider content background color', 'newspapers' ),
	'section'     => 'newspapers_homepage_slidersettings',
	'default'     => 'rgba(0,0,0,0.14)',
  'transport'   => 'auto',
  'choices'     => array(
		'alpha' => true,
	),
  'output'      => array(
    array(
      'element' => '.slider-container .post-header-outer',
      'property' => 'background',
      'units'   => '',
    ),
  ),
) );

Kirki::add_field('newspapers', array(
    'type' => 'multicolor',
    'settings' => 'slidertext_color',
    'label' => esc_attr__('Slider content Color ', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'text' => esc_attr__('Text color', 'newspapers'),
        'catbg' => esc_attr__('Category color', 'newspapers')
    ),
    'default' => array(
        'text' => '#fff',
        'catbg' => '#A683F5'

    ),
    'output' => array(
        array(
            'choice' => 'text',
            'element' => '.slider-container .post-header .post-title a,.slider-container .post-meta-info .meta-info-el a,.slider-container .meta-info-date,.slider-right .post-header .post-title a,.slider-right .meta-info-date,.slider-right .post-meta-info .meta-info-author .author',
            'property' => 'color'
        ),
        array(
            'choice' => 'catbg',
            'element' => '.slider-container .cat-info-el,.slider-right .post-header .post-cat-info .cat-info-el',
            'property' => 'background-color'
        )

    )
));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'sliderbg_color',
    'label' => esc_attr__('Change Slider background-color', 'newspapers'),
    'section' => 'newspapers_homepage_slidersettings',
    'default' => '#fff',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' #top-content,#top-content .flexslider',
            'property' => 'background',
            'units' => ''
        ),
        array(
            'element' => ' #top-content .flexslider',
            'property' => 'border-color',
            'units' => ''
        )

    )
));

/* * * * * * * Site Color options * * * * * * */
$newspapers_flavor_color = get_theme_mod('newspapers_flavor_color', '#00bcd4');
$box_shadow              = Kirki_Color::get_rgba($newspapers_flavor_color, .30);

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_flavor_color',
    'label' => esc_attr__('Flavor Color', 'newspapers'),
    'section' => 'newspapers_maincolor_settings',
    'default' => '#00bcd4',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.comment-title h2,h2.comment-reply-title,.sidebar-inner .widget_archive ul li a::before, .sidebar-inner .widget_categories ul li a::before, .sidebar-inner .widget_pages ul li a::before, .sidebar-inner .widget_nav_menu ul li a::before, .sidebar-inner .widget_portfolio_category ul li a::before,.defult-text a span,.woocommerce .star-rating span::before',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '.woocommerce nav.woocommerce-pagination ul li span.current,.woocommerce ul.products li.product .button,.tagcloud a,.lates-post-warp .button.secondary,.pagination .current,.pagination li a,.widget_search .search-submit,.author-links a,.comment-form .form-submit input#submit, a.box-comment-btn,.comment-form .form-submit input[type="submit"],.cat-info-el,.comment-list .comment-reply-link,.woocommerce div.product form.cart .button, .woocommerce #respond input#submit.alt,.woocommerce a.button.alt, .woocommerce button.button.alt,.woocommerce input.button.alt, .woocommerce #respond input#submit,.woocommerce a.button, .woocommerce button.button, .woocommerce input.button,.mobile-menu  .nav-bar .offcanvas-trigger',
            'property' => 'background',
            'units' => ''
        ),
        array(
            'element' => '.viewall-text .shadow',
            'property' => 'box-shadow',
            'value_pattern' => '0 2px 2px 0 ' . $box_shadow . ', 0 2px 8px 0 ' . $box_shadow . ''

        ),
        array(
            'element' => '.woocommerce .button',
            'property' => 'box-shadow',
            'value_pattern' => '0 2px 2px 0 ' . $box_shadow . ', 0 3px 1px -2px ' . $box_shadow . ', 0 1px 5px 0 ' . $box_shadow . '',
            'units' => '!important'
        ),
        array(
            'element' => '.woocommerce .button:hover',
            'property' => 'box-shadow',
            'value_pattern' => '-1px 11px 23px -4px ' . $box_shadow . ',1px -1.5px 11px -2px  ' . $box_shadow . '',
            'units' => '!important'
        )
    )
));

$newspapers_hover_color = get_theme_mod('newspapers_hover_color', '#2f2f2f');
$box_shadow_hover       = Kirki_Color::get_rgba($newspapers_hover_color, .43);

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_hover_color',
    'label' => esc_attr__('Hover Color', 'newspapers'),
    'section' => 'newspapers_maincolor_settings',
    'default' => '#2f2f2f',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.tagcloud a:hover,.post-title a:hover,.single-nav .nav-left a:hover, .single-nav .nav-right a:hover,.comment-title h2:hover,h2.comment-reply-title:hover,.meta-info-comment .comments-link a:hover,.woocommerce div.product div.summary a:hover',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '.tagcloud a:hover,.viewall-text .button.secondary:hover,.single-nav a:hover>.newspaper-nav-icon,.pagination .current:hover,.pagination li a:hover,.widget_search .search-submit:hover,.author-links a:hover,.comment-form .form-submit input#submit:hover, a.box-comment-btn:hover, .comment-form .form-submit input[type="submit"]:hover,.cat-info-el:hover,.comment-list .comment-reply-link:hover',
            'property' => 'background',
            'units' => ''
        ),
        array(
            'element' => '.viewall-text .shadow:hover',
            'property' => 'box-shadow',
            'value_pattern' => '-1px 11px 15px -8px ' . $box_shadow_hover . ''
        )
    )
));


Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'sectionwidgets_title_color',
    'label' => esc_attr__('Change font-page section title and widgets Title color', 'newspapers'),
    'section' => 'newspapers_maincolor_settings',
    'default' => '#0a0a0a',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => ' .block-header-wrap .block-title,.widget-title h3',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => ' .block-header-wrap .block-title h3,.widget-title h3',
            'property' => 'border-bottom-color',
            'units' => ''
        )
    )
));



/* * * * * * * Post page setting * * * * * * */

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'on_of_postpagesubhead',
    'label' => esc_attr__('enabled/disabled Header', 'newspapers'),
    'section' => 'newspapers_postpage_settings',
    'default' => '1',
    'priority' => 10
));

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'sub_header_postpage',
    'label' => esc_attr__('Select Sub-header Style', 'newspapers'),
    'section' => 'newspapers_postpage_settings',
    'default' => 'gradient_subheader',
    'priority' => 10,
    'choices' => array(
        'img_subheader' => esc_attr__('Sub Header Image', 'newspapers'),
        'gradient_subheader' => esc_attr__('Sub Header gradient', 'newspapers')
    ),
    'active_callback' => array(
        array(
            'setting' => 'on_of_postpagesubhead',
            'operator' => '==',
            'value' => true
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'palette',
    'settings' => 'subheader_post_gradient',
    'label' => esc_attr__('Gradient color', 'newspapers'),
    'section' => 'newspapers_postpage_settings',
    'default' => 'gradient4',
    'priority' => 10,
    'choices' => array(
        'gradient1' => array(
            'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%);'
        ),
        'gradient2' => array(
            'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%);'
        ),
        'gradient3' => array(
            'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);'
        ),
        'gradient4' => array(
            'radial-gradient(circle 248px at center, #16d9e3 0%, #30c7ec 47%, #46aef7 100%);'
        ),
        'gradient5' => array(
            'linear-gradient(to top, #09203f 0%, #537895 100%);'
        ),
        'gradient6' => array(
            'linear-gradient(to top, #f77062 0%, #fe5196 100%);'
        ),
        'gradient7' => array(
            'linear-gradient(-45deg, #f857a6, #ff5858);'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'sub_header_postpage',
            'operator' => '==',
            'value' => 'gradient_subheader'
        )
    ),

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'subheader_text_color',
    'label' => esc_attr__('Sub-header text Color', 'newspapers'),
    'section' => 'newspapers_postpage_settings',
    'default' => '#fff',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#sub_banner .breadcrumb-wraps .breadcrumbs li,#sub_banner .heade-content h1,.heade-content h1,.breadcrumbs li,.breadcrumbs a,.breadcrumbs li:not(:last-child)::after',
            'property' => 'color',
            'units' => ''
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'on_of_postpagesubhead',
            'operator' => '==',
            'value' => true
        )
    )
));


Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_post_page',
    'section' => 'newspapers_postpage_settings',
    'priority' => 10,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Layout options', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'radio-image',
    'settings' => 'sidbar_position',
    'label' => esc_html__('Layout Sidebar', 'newspapers'),
    'section' => 'newspapers_postpage_settings',
    'default' => 'right',
    'priority' => 10,
    'choices' => array(
        'right' => get_template_directory_uri() . '/images/sidebar-right.png',
        'full' => get_template_directory_uri() . '/images/full-width.png',
        'left' => get_template_directory_uri() . '/images/sidebar-left.png'
    )
));
/*----------- adding single Post Options-----------*/

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'show_single_ftimage',
    'label' => esc_html__('Hide/Show Feature Image', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10
));

Kirki::add_field('newspapers', array(
    'type' => 'custom',
    'settings' => 'themezwp_seperator_post_page',
    'section' => 'newspapers_singlepost_settings',
    'priority' => 10,
    'default' => '<h2 class="themezwp-kirki-seperator">' . esc_html__('Meta data Hide/Show option ', 'newspapers') . '</h2>'
));

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'show_single_breadcrumb',
    'label' => esc_html__('Hide/Show Breadcrumb', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10
));

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'show_single_cat',
    'label' => esc_html__('Hide/Show Category', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10
));


Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_author',
    'label' => esc_html__('Hide/Show Author', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_date',
    'label' => esc_attr__('Date', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_comments',
    'label' => esc_attr__('Comments', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_tags',
    'label' => esc_attr__('Tags', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_authorbio',
    'label' => esc_attr__('Author Bio', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_authorbio',
    'label' => esc_attr__('Author Bio', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'newspapers_show_single_related',
    'label' => esc_attr__('Related Post', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => '1',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));
Kirki::add_field('newspapers', array(
    'type' => 'text',
    'settings' => 'related_post_title',
    'label' => esc_attr__('Related Post title', 'newspapers'),
    'section' => 'newspapers_singlepost_settings',
    'default' => esc_attr__('You Might Also Like', 'newspapers'),
    'priority' => 10,
    'transport' => 'postMessage',
    'js_vars' => array(
        array(
            'element' => '.single-post-box-related .block-header-inner h3 ',
            'function' => 'html'
        )
    ),
    'active_callback' => array(
        array(
            'setting' => 'newspapers_show_single_related',
            'operator' => '==',
            'value' => true
        )
    )
));

/*=============================================>>>>>
= Page Options =
===============================================>>>>>*/

Kirki::add_field('newspapers', array(
    'type' => 'checkbox',
    'settings' => 'show_page_subheader',
    'label' => esc_html__('Hide/Show Page Header', 'newspapers'),
    'section' => 'newspapers_singlepage_settings',
    'default' => '1',
    'priority' => 10
));

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'sub_header_page',
    'label' => esc_attr__('Select Sub-header Style', 'newspapers'),
    'section' => 'newspapers_singlepage_settings',
    'default' => 'gradient_subheader',
    'priority' => 10,
    'choices' => array(
        'img_subheader' => esc_attr__('Sub Header Image', 'newspapers'),
        'gradient_subheader' => esc_attr__('Sub Header gradient', 'newspapers')
    ),
    'active_callback' => array(
        array(
            'setting' => 'show_page_subheader',
            'operator' => '==',
            'value' => true
        )
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'palette',
    'settings' => 'page_subheader_gradient',
    'label' => esc_attr__('Gradient color', 'newspapers'),
    'section' => 'newspapers_singlepage_settings',
    'default' => 'gradient4',
    'priority' => 10,
    'choices' => array(
        'gradient1' => array(
            'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%);'
        ),
        'gradient2' => array(
            'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%);'
        ),
        'gradient3' => array(
            'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);'
        ),
        'gradient4' => array(
            'radial-gradient(circle 248px at center, #16d9e3 0%, #30c7ec 47%, #46aef7 100%);'
        ),
        'gradient5' => array(
            'linear-gradient(to top, #09203f 0%, #537895 100%);'
        ),
        'gradient6' => array(
            'linear-gradient(to top, #f77062 0%, #fe5196 100%);'
        )
    ),

    'active_callback' => array(
        array(
            'setting' => 'sub_header_page',
            'operator' => '==',
            'value' => 'gradient_subheader'
        )
    ),
));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'subheader_text_colorpage',
    'label' => esc_attr__('Page title text Color', 'newspapers'),
    'section' => 'newspapers_singlepage_settings',
    'default' => '#0a0a0a',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => 'sub_header_page .heade-content h1',
            'property' => 'color',
            'units' => ''
        )
    )

));

/*=============================================>>>>>
= Footer options =
===============================================>>>>>*/

/*----------- footer widgets -----------*/
Kirki::add_field('newspapers', array(
    'type' => 'switch',
    'settings' => 'sticky_footer',
    'label' => __('sticky footer on/off', 'newspapers'),
    'section' => 'newspapers_footerwid_settings',
    'default' => '0',
    'priority' => 10,
    'choices' => array(
        'on' => esc_attr__('Enable', 'newspapers'),
        'off' => esc_attr__('Disable', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'select',
    'settings' => 'footerwid_row_control',
    'label' => __('Displaying Widgets in a row', 'newspapers'),
    'section' => 'newspapers_footerwid_settings',
    'default' => 'large-4',
    'priority' => 10,
    'transport' => 'postMessage',
    'choices' => array(
        'large-12' => esc_attr__('1 widgets', 'newspapers'),
        'large-6' => esc_attr__('2 widgets', 'newspapers'),
        'large-4' => esc_attr__('3 widgets', 'newspapers'),
        'large-3' => esc_attr__('4 widgets', 'newspapers')
    )
));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'footerwid_bg_color',
    'label' => esc_attr__('Widget section Background Color', 'newspapers'),
    'section' => 'newspapers_footerwid_settings',
    'default' => '#282828',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .top-footer-wrap',
            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'footerwid_title_color',
    'label' => esc_attr__('Widget title Color', 'newspapers'),
    'section' => 'newspapers_footerwid_settings',
    'default' => '#e3e3e3',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .block-header-wrap .block-title h3,#footer .widget-title h3',
            'property' => 'color',
            'units' => ''
        ),
        array(
            'element' => '#footer .block-header-wrap .block-title h3,#footer .widget-title h3',
            'property' => 'border-bottom-color',
            'units' => ''
        )
    )

));

/*----------- Footer COPYRIGHT options -----------*/

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'news_copyright_bgcolor',
    'label' => esc_attr__('Copyright background color', 'newspapers'),
    'section' => 'newspapers_copyright_settings',
    'default' => '#242424',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .footer-copyright-wrap',
            'property' => 'background-color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'news_copyright_textcolor',
    'label' => esc_attr__('Copyright Text color', 'newspapers'),
    'section' => 'newspapers_copyright_settings',
    'default' => '#fff',
    'transport' => 'auto',
    'priority' => 10,
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '#footer .footer-copyright-text,.footer-copyright-text p,.footer-copyright-text li,.footer-copyright-text ul,.footer-copyright-text ol',
            'property' => 'color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'news_copyright_textcolor',
    'label' => esc_attr__('Copyright Text color', 'newspapers'),
    'section' => 'newspapers_copyright_settings',
    'default' => '#e5e5e5',
    'transport' => 'auto',
    'priority' => 10,
    'output' => array(
        array(
            'element' => '.footer-copyright-text a',
            'property' => 'color',
            'units' => ''
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'editor',
    'settings' => 'news_copyright_text',
    'label' => __('Copyright text', 'newspapers'),
    'section' => 'newspapers_copyright_settings',
    'priority' => 10,
    'transport' => 'postMessage',
    'js_vars' => array(
        array(
            'element' => '.footer-copyright-text ,.footer-copyright-text p,.footer-copyright-text h1,.footer-copyright-text li,.footer-copyright-text ul',
            'function' => 'html'
        )
    )
));

// TODO: will add woocommerce options


Kirki::add_field('newspapers', array(
    'type' => 'toggle',
    'settings' => 'newspapers_woocommerce_title',
    'label' => esc_attr__('Woocommerce title Enable/Disable', 'newspapers'),
    'section' => 'newspapers_woocommercecustom_settings',
    'default' => '1',
    'priority' => 10
));

/*----------- gradient color header woocommerce -----------*/

Kirki::add_field('newspapers', array(
    'type' => 'palette',
    'settings' => 'woocommerce_header_gradient',
    'label' => esc_attr__('Gradient color', 'newspapers'),
    'section' => 'newspapers_woocommercecustom_settings',
    'default' => 'linear-gradient(to top, #f77062 0%, #fe5196 100%);',
    'priority' => 10,
    'transport' => 'auto',
    'choices' => array(
        'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%);' => array(
            'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%);'
        ),
        'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%);' => array(
            'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%);'
        ),
        'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);' => array(
            'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);'
        ),
        'radial-gradient(circle 248px at center, #16d9e3 0%, #30c7ec 47%, #46aef7 100%);' => array(
            'radial-gradient(circle 248px at center, #16d9e3 0%, #30c7ec 47%, #46aef7 100%);'
        ),
        'linear-gradient(to top, #09203f 0%, #537895 100%);' => array(
            'linear-gradient(to top, #09203f 0%, #537895 100%);'
        ),
        'linear-gradient(to top, #f77062 0%, #fe5196 100%);' => array(
            'linear-gradient(to top, #f77062 0%, #fe5196 100%);'
        ),
        'linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%);' => array(
            'linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%);'
        ),
        'linear-gradient(-20deg, #fc6076 0%, #ff9a44 100%);' => array(
            'linear-gradient(-20deg, #fc6076 0%, #ff9a44 100%);'
        )
    ),
    'output' => array(
        array(
            'element' => '.woo-header-newspapers',
            'property' => 'background'
        )
    )

));

Kirki::add_field('newspapers', array(
    'type' => 'color',
    'settings' => 'newspapers_woocommerce_extcolor',
    'label' => __('Header text color', 'newspapers'),
    'section' => 'newspapers_woocommercecustom_settings',
    'default' => '#020202',
    'choices' => array(
        'alpha' => true
    ),
    'output' => array(
        array(
            'element' => '.heade-content.woo-header-newspapers h1,.woocommerce .woocommerce-breadcrumb a,.woocommerce .breadcrumbs li',
            'property' => 'color'
        )
    )
));
