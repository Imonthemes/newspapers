<?php
/**
 * Add sections
 */

 /*----------- adding Page section -----------*/

 Kirki::add_section( 'newspapers_upgradepro_options', array(
     'title'          =>esc_attr__( 'About Theme Info ', 'newspapers' ),
      'panel'          => 'upgradepro_options', // Not typically needed.
     'priority'       => 1,
     'type'           => 'expanded',
     'capability'     => 'edit_theme_options',
 ) );

/* adding Header Options section*/
Kirki::add_section( 'newspapers_topheader_settings', array(
    'title'          =>esc_attr__( 'Top Header Settings', 'newspapers' ),
     'panel'          => 'header_options', // Not typically needed.
    'priority'       => 1,


) );

Kirki::add_section( 'newspapers_headtitle_settings', array(
    'title'          =>esc_attr__( 'Logo and header layout', 'newspapers' ),
     'panel'          => 'header_options', // Not typically needed.
    'priority'       => 1,


) );
Kirki::add_section( 'newspapers_mainmenu_settings', array(
    'title'          =>esc_attr__( 'Main Menu Styling', 'newspapers' ),
     'panel'          => 'header_options', // Not typically needed.
    'priority'       => 2,


) );

Kirki::add_section( 'newspapers_subheader_settings', array(
    'title'          =>esc_attr__( 'Sub-header or page title', 'newspapers' ),
     'panel'          => 'header_options', // Not typically needed.
    'priority'       => 2,


) );

/* adding Homepage Options section*/

Kirki::add_section( 'newspapers_homepage_slidersettings', array(
    'title'          =>esc_attr__( 'Slider', 'newspapers' ),
     'panel'          => 'homepage_options', // Not typically needed.
    'priority'       => 1,

) );

Kirki::add_section( 'newspapers_homepagecolor_settings', array(
    'title'          =>esc_attr__( 'Color', 'newspapers' ),
     'panel'          => 'homepage_options', // Not typically needed.
    'priority'       => 1,

) );

Kirki::add_section( 'newspapers_homepagetypho_settings', array(
    'title'          =>esc_attr__( 'Typography', 'newspapers' ),
     'panel'          => 'homepage_options', // Not typically needed.
    'priority'       => 1,

) );

/* adding General Options section*/

Kirki::add_section( 'newspapers_bglayout_settings', array(
    'title'          =>esc_attr__( 'Background color ', 'newspapers' ),
     'panel'          => 'bglayout_options', // Not typically needed.
    'priority'       => 3,

) );

Kirki::add_section( 'newspapers_mainlayout_settings', array(
    'title'          =>esc_attr__( 'Site Layout ', 'newspapers' ),
     'panel'          => 'bglayout_options', // Not typically needed.
    'priority'       => 3,

) );

Kirki::add_section( 'newspapers_maintypography_settings', array(
    'title'          =>esc_attr__( 'Site typography ', 'newspapers' ),
     'panel'          => 'bglayout_options', // Not typically needed.
    'priority'       => 3,

) );

Kirki::add_section( 'newspapers_maincolor_settings', array(
    'title'          =>esc_attr__( 'Site Color options ', 'newspapers' ),
     'panel'          => 'bglayout_options', // Not typically needed.
    'priority'       => 3,

) );

/*----------- adding Post Options section -----------*/

Kirki::add_section( 'newspapers_postpage_settings', array(
    'title'          =>esc_attr__( 'categories-archive-blog etc. page options ', 'newspapers' ),
     'panel'          => 'newspaperspost_options', // Not typically needed.
    'priority'       => 1,

) );

Kirki::add_section( 'newspapers_singlepost_settings', array(
    'title'          =>esc_attr__( 'Single Post settings', 'newspapers' ),
     'panel'          => 'newspaperspost_options', // Not typically needed.
    'priority'       => 1,

) );

/*----------- adding Page section -----------*/

Kirki::add_section( 'newspapers_singlepage_settings', array(
    'title'          =>esc_attr__( 'Page settings ', 'newspapers' ),
     'panel'          => 'newspaperspage_options', // Not typically needed.
    'priority'       => 1,
    'type'           => 'expanded',
    'capability'     => 'edit_theme_options',
) );
/*----------- adding footer section -----------*/

Kirki::add_section( 'newspapers_footerwid_settings', array(
    'title'          =>esc_attr__( 'Footer Widgets options ', 'newspapers' ),
     'panel'          => 'newspapersfooter_options', // Not typically needed.
    'priority'       => 1,
) );
Kirki::add_section( 'newspapers_copyright_settings', array(
    'title'          =>esc_attr__( 'Footer Copyright options ', 'newspapers' ),
     'panel'          => 'newspapersfooter_options', // Not typically needed.
    'priority'       => 1,
) );

/*----------- adding woocommerce options -----------*/

Kirki::add_section( 'newspapers_woocommercecustom_settings', array(
    'title'          =>esc_attr__( 'Woocommerce settings ', 'newspapers' ),
     'panel'          => 'woocommercecustom_options', // Not typically needed.
    'priority'       => 1,
    'type'           => 'expanded',
    'capability'     => 'edit_theme_options',
) );
