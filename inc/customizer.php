<?php
/**
* newspapers Theme Customizer
*
* @package newspapers
*/


/**
* Add postMessage support for site title and description for the Theme Customizer.
*
* @param WP_Customize_Manager $wp_customize Theme Customizer object.
*/

function newspapers_customize_register($wp_customize)
{
    $wp_customize->get_setting('blogname')->transport         = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
  /*  $wp_customize->add_control('header_image')->theme_supports=false; */


    /*----------- Move Customizer default Control -----------*/
    $newspapers_bglayout_settings = $wp_customize->get_control('background_color');
    if ($newspapers_bglayout_settings) {
        $newspapers_bglayout_settings->section = 'newspapers_bglayout_settings';
    }

    $header_options = $wp_customize->get_section('header_image');
    if (! empty($header_options)) {
        $header_options->panel = 'header_options';
        $header_options->title   = __('Header Image', 'newspapers');
        $header_options->priority = 2;
    }
    $static_front_page = $wp_customize->get_section('static_front_page');
    if (! empty($static_front_page)) {
        $static_front_page->panel = 'homepage_options';
        $static_front_page->title   = __('Homepage Settings', 'newspapers');
        $static_front_page->priority = 1;
    }
    /*----------- Add section for widgets -----------*/

    $headeradvertising_section = $wp_customize->get_section('sidebar-widgets-sidebar-headeradvertising');
    if (! empty($headeradvertising_section)) {
        $headeradvertising_section->panel = 'header_options';
        $headeradvertising_section->title   = __('Header advertising area', 'newspapers');
        $headeradvertising_section->priority = 2;
    }
    $homewidgetsarea_section = $wp_customize->get_section('sidebar-widgets-sidebar-homepagewidgets');
    if (! empty($homewidgetsarea_section)) {
        $homewidgetsarea_section->panel = 'homepage_options';
        $homewidgetsarea_section->title   = __('Home page widgets area', 'newspapers');
        $homewidgetsarea_section->priority = 2;
    }
    $homesidebar_section = $wp_customize->get_section('sidebar-widgets-sidebar-homepagesidebar');
    if (! empty($homesidebar_section)) {
        $homesidebar_section->panel = 'homepage_options';
        $homesidebar_section->title   = __('Home page Sidebar', 'newspapers');
        $homesidebar_section->priority = 2;
    }
    $newspapersfooter_options = $wp_customize->get_section('sidebar-widgets-foot_sidebar');
    if (! empty($newspapersfooter_options)) {
        $newspapersfooter_options->panel = 'newspapersfooter_options';
        $newspapersfooter_options->title   = __('Footer Widgets', 'newspapers');
        $newspapersfooter_options->priority = 2;
    }
}
add_action('customize_register', 'newspapers_customize_register');


/**
* Sets up the WordPress core custom header .
*
* @see advance_header_style()
*/
function newspapers_custom_header()
{
    /**
    * Filter the arguments used when adding 'custom-header' support in advance.
    *
    *
    * @param array $args {
    *     An array of custom-header support arguments.
    *
    *     @type string $default-text-color Default color of the header text.
    *     @type int      $width            Width in pixels of the custom header image. Default 1200.
    *     @type int      $height           Height in pixels of the custom header image. Default 280.
    *     @type bool     $flex-height      Whether to allow flexible-height header images. Default true.
    *     @type callable $wp-head-callback Callback function used to style the header image and text
    *                                      displayed on the blog.
    * }
    */
    add_theme_support('custom-header', apply_filters('newspapers_custom_header_args', array(
      'flex-width'    => true,
      'width'                  => 1800,
      'height'                 => 250,
    )));
}
add_action('after_setup_theme', 'newspapers_custom_header');


/**
* Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
*/
function newspapers_customize_preview_js()
{
    wp_enqueue_script('newspapers_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true);
}
add_action('customize_preview_init', 'newspapers_customize_preview_js');

function newspapers_registers()
{
    wp_enqueue_style('newspapers_customizer_style', get_template_directory_uri() . '/css/admin.css', 'newspapers-style', true);
    wp_enqueue_script('newspapers-customizer-js',get_template_directory_uri().'/js/customizer-controls.js', array('customize-controls'), true);

}
add_action('customize_controls_enqueue_scripts', 'newspapers_registers');


require get_template_directory() . '/inc/customizer/config.php';
require get_template_directory() . '/inc/customizer/panels.php';
require get_template_directory() . '/inc/customizer/sections.php';
require get_template_directory() . '/inc/customizer/fields.php';
