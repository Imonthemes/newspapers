<?php
/**
 * newspapers demos
 *
 * @package themezwp
 * @subpackage newspapers
 * @since newspapers 1.0.0
 */

 function newspapers_import_files() {

	return array(
		array(
			'import_file_name'             => 'Demo1',
			'categories'                   => array( 'Default ' ),
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/demo1.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/demo1.wie',
			'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/demo1.dat',
      'import_preview_image_url'   => esc_url(get_template_directory_uri().'/inc/demo/demo1final.png'),
			'preview_url'                  => 'http://themezwp.com/newspapers-demo/demo1',
		),
    array(
			'import_file_name'             => 'Demo1',
			'categories'                   => array( 'Fullwidth ' ),
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/demo2.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/demo2.wie',
			'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/demo2.dat',
      'import_preview_image_url'   => esc_url(get_template_directory_uri().'/inc/demo/demo2final.png'),
      'preview_url'                  => 'http://themezwp.com/newspapers-demo/demo2/',

		),
	);
}
add_filter( 'pt-ocdi/import_files', 'newspapers_import_files' );

add_action( 'pt-ocdi/enable_wp_customize_save_hooks', '__return_true' );

add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );

if ( ! function_exists( 'newspapers_after_import' ) ) :
function newspapers_after_import( ) {

        //Set Menu
        $top_menu = get_term_by('name', 'custom', 'nav_menu');
        set_theme_mod( 'nav_menu_locations' , array(
              'primary' => $top_menu->term_id,
             )
        );
        //Set Front page
    $page = get_page_by_title( 'Home');
    if ( isset( $page->ID ) ) {
     update_option( 'page_on_front', $page->ID );
     update_option( 'show_on_front', 'page' );
    }

}
add_action( 'pt-ocdi/after_import', 'newspapers_after_import' );
endif;
