<?php

if (! function_exists('newspapers_get_sidebar')) {
    /**
    * Display storefront sidebar
    *
    * @uses get_sidebar()
    * @since 1.0.0
    */
    function newspapers_get_sidebar()
    {
        get_sidebar();
    }
}

/**
* Use front-page.php when Front page displays is set to a static page.
*
* @since newspapers 1.0
*
* @param string $template front-page.php.
*
* @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
*/
function newspapers_front_page_template($template)
{
    return is_home() ? '' : $template;
}
add_filter('frontpage_template', 'newspapers_front_page_template');


/**
* Link all post thumbnails to the post permalink.
*
* @param string $html          Post thumbnail HTML.
* @param int    $post_id       Post ID.
* @param int    $post_image_id Post image ID.
* @return string Filtered post image HTML.
*/
function newspapers_post_image_html($html, $post_id, $post_image_id)
{
    $html = '<a href="' . esc_url(get_permalink($post_id)) . '">' . $html . '</a>';
    return $html;
}
add_filter('post_thumbnail_html', 'newspapers_post_image_html', 10, 3);

/**
 * Category list prin
 * @var $categories
 */
if (! function_exists('newspapers_category_list')) :
function newspapers_category_list()
{
    $categories = get_the_category();
    $separator = ' ';
    $output = '';
    if (! empty($categories)) {
        foreach ($categories as $category) {
            $output .=
'<a class="cat-info-el" href="' . esc_url(get_category_link($category->term_id)) .
'" alt="' . esc_attr(sprintf(__('View all posts in %s', 'newspapers'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
        }
        echo trim($output, $separator);
    }
}
endif;

/**
* Prints first category link and name
*/
if (! function_exists('newspapers_firstcategory_link')) :
function newspapers_firstcategory_link()
{
    $categories = get_the_category();
    if (! empty($categories)) {
        echo  '<a class="cat-info-el" href="' . esc_url(get_category_link($categories[0]->term_id)) . '">' . esc_html($categories[0]->name) . '</a>';
    }
}
endif;

/**
* comments meta
*/
if (! function_exists('newspapers_meta_comment')) :
function newspapers_meta_comment()
{
    if (! post_password_required() && (comments_open() || get_comments_number())) {
        echo '<span class="comments-link">';
        /* translators: %s: post title */
        comments_popup_link(sprintf(wp_kses(__('Leave a Comment<span class="screen-reader-text"> on %s</span>', 'newspapers'), array( 'span' => array( 'class' => array() ) )), get_the_title()));
        echo '</span>';
    }
}
endif;


if (! function_exists('newspapers_meta_tag')) :
/**
* Prints HTML with meta information for the tags .
*/
function newspapers_meta_tag()
{
    // Hide category and tag text for pages.
    if ('post' === get_post_type()) {
        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list();
        if ($tags_list) {
            echo '<span class="single-tag-text">';
            _e('Tagged:', 'newspapers');
            echo '</span>';
            echo $tags_list;
        }
    }
}
endif;

if (! function_exists('newspapers_edit_link')) :
/**
* Prints HTML with meta information for the tags .
*/
function newspapers_edit_link()
{
    edit_post_link(
										sprintf(
										/* translators: %s: Name of current post */
										esc_html__('Edit %s', 'newspapers'),
										the_title('<span class="screen-reader-text">"', '"</span>', false)
									)
		);
}
endif;


/**
* newspapers gradient color set .
*/
if (! function_exists('newspapers_gradient_color')) :

function newspapers_gradient_color()
{
    if (is_page()) {
        $saved_palette = get_theme_mod('page_subheader_gradient', 'gradient1');
    } else {
        $saved_palette = get_theme_mod('subheader_post_gradient', 'gradient1');
    }

    if ('gradient1' == $saved_palette) {
        $background_gradient   = 'linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%)';
    }

    if ('gradient2' == $saved_palette) {
        $background_gradient   = 'linear-gradient(120deg, #84fab0 0%, #8fd3f4 100%)';
    }
    if ('gradient3' == $saved_palette) {
        $background_gradient   = 'linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%)';
    }
    if ('gradient4' == $saved_palette) {
        $background_gradient   = 'radial-gradient(circle 248px at center, #16d9e3 0%, #30c7ec 47%, #46aef7 100%)';
    }
    if ('gradient5' == $saved_palette) {
        $background_gradient   = 'linear-gradient(to top, #09203f 0%, #537895 100%)';
    }
    if ('gradient6' == $saved_palette) {
        $background_gradient   = 'linear-gradient(to top, #f77062 0%, #fe5196 100%)';
    }
    if ('gradient7' == $saved_palette) {
        $background_gradient   = 'linear-gradient(-45deg, #f857a6, #ff5858)';
    }

    $styles = "background-image:{$background_gradient};";
    return $styles;
}

endif;


/**
 * newspapers Post Page subheader .
 */
if (! function_exists('subheader_post') ) :
function subheader_post()
{ ?>
   <?php $sub_header_style = get_theme_mod( 'sub_header_postpage', 'gradient_subheader' );?>
     <div id="sub_banner" class="postpage_subheader" <?php if ( 'gradient_subheader' == $sub_header_style ) : ?> style="<?php echo newspapers_gradient_color(); ?>" <?php endif;?>>
     <div class="grid-container">
       <div class="grid-x grid-padding-x ">
         <div class="cell small-12 ">
           <div class="heade-content">
             <h1 class="text-center">
               <?php if ( is_category()  ) : ?>
							 <?php $category_title = single_cat_title("", false); ?>
               <?php if ( ! empty($category_title)) : ?>
                 <?php echo  esc_html($category_title);?>
               <?php endif; ?>
               <?php endif; ?>

                <?php if (  is_author()  ) : ?>
               <?php
                /**
                 * Filter the newspapers author bio avatar size.
                 *
                 * @since newspapers
                 *
                 * @param int $size The avatar height and width size in pixels.
                 */
                $author_bio_avatar_size = apply_filters( 'newspapers_author_bio_avatar_size', 42 );

                echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
              ?>
                <?php esc_attr__('Posts by ', 'newspapers');?><?php echo get_the_author(); ?>
                <?php endif; ?>
                <?php if (  is_search()   ) : ?>
              <?php printf( esc_attr__( 'Search Results for: %s', 'newspapers' ), '<span>' . esc_html( get_search_query() ) . '</span>'); ?>
                <?php endif; ?>

                <?php if ( is_front_page() && is_home() ) {
                  // Notitle Default homepage
                } elseif ( is_home() ) {
                  // blog page
                	$our_title = get_the_title( get_option('page_for_posts', true) );
                  echo	esc_attr($our_title) ;
                }?>
             </h1>
						 <div class="breadcrumb-wraps center-conetent"><?php newspapers_breadcrumb();?> </div>
           </div>
         <?php   if ( ('img_subheader' == $sub_header_style ) && get_header_image()) : ?>
             <div class="header-image-container"style="background:url(<?php header_image(); ?>);" >
             </div>
             <div class="overlay"></div>
           <?php endif;?>

         </div>
       </div>
     </div>
   </div>
<?php }
endif;

if (! function_exists('subheader_page') && ( true == get_theme_mod( 'show_page_subheader', true ) ) ) :
function subheader_page()
{ ?>
	<?php $sub_header_style = get_theme_mod( 'sub_header_page', 'gradient_subheader' );?>
<div id="sub_banner" class="sub_header_page"<?php   if ( ('gradient_subheader' == $sub_header_style ) ): ?> style="<?php echo newspapers_gradient_color(); ?>" <?php endif;?>>
  <div class="grid-container">
    <div class="grid-x grid-padding-x ">
      <div class="cell small-12 ">
        <div class="heade-content">
          <h1 class="text-center">
            <?php the_title(); ?>
          </h1>
        </div>
				<?php $sub_header_style = get_theme_mod( 'sub_header_page', 'img_subheader' );?>
				<?php $page_id = get_queried_object_id();?>
				<?php   if ( ('img_subheader' == $sub_header_style ) ): ?>
        <?php
        // If a featured image is set, insert into layout and use Interchange
        // to select the optimal image size per named media query.
        if ( has_post_thumbnail($page_id ) ) : ?>
          <div  class="header-image-container" role="banner" data-interchange="[<?php echo esc_url(the_post_thumbnail_url('newspapers-small')); ?>, small], [<?php echo esc_url(the_post_thumbnail_url('newspapers-large')); ?>, medium], [<?php echo esc_url(the_post_thumbnail_url('newspapers-xlarge')); ?>, large], [<?php echo esc_url(the_post_thumbnail_url('newspapers-xlarge')); ?>, xlarge]" >
          </div>
          <div class="overlay"></div>
				<?php else :?>
					<?php if ( is_customize_preview() && current_user_can( 'edit_theme_options' )): ?>
						<button class="preview radius z-depth-2 button expanded">
						<?php echo esc_attr__('For add Header Image => Page => Edit  and Set featured image','newspapers')?> </button>
					<?php endif;?>
        <?php endif;?>
				<?php endif;?>
      </div>
    </div>
  </div>
</div>
<?php }
endif;
/**
 * newspapers Logo position .
 */

if (! function_exists('newspapers_logo_position')) :

function newspapers_logo_position()
{
    $logo_position = get_theme_mod('logo_position', 'left');
    if ('left' == $logo_position) {
        $layout   = '';
    } elseif ('center' == $logo_position) {
        $layout   = '';
    } elseif ('right' == $logo_position) {
        $layout   = 'large-order-2';
    }
    $position = $layout;
    return $position;
}
endif;

/*----------- sidebar layout -----------*/

if (! function_exists('newspapers_sidebar_layout')) :
function newspapers_sidebar_layout()
{
    $sidbar_position = get_theme_mod('sidbar_position', 'right');
    if ((!is_active_sidebar('right-sidebar') || 'full' == $sidbar_position)) {
        $siderbar='large-11 medium-11';
    } elseif (is_active_sidebar('right-sidebar') && ('right' == $sidbar_position)) {
        $siderbar='large-8';
    } elseif (is_active_sidebar('right-sidebar') && ('left' == $sidbar_position)) {
        $siderbar='large-8 large-order-2';
    }
    $siderbars = $siderbar;
    return $siderbars;
}
endif;


/**
 * Enable Foundation responsive embeds for WP video embeds
 */
if ( ! function_exists( 'newspapers_responsive_video_oembed_html' ) ) :
	function newspapers_responsive_video_oembed_html( $html, $url, $attr, $post_id ) {
		// Whitelist of oEmbed compatible sites that **ONLY** support video.
		// Cannot determine if embed is a video or not from sites that
		// support multiple embed types such as Facebook.
		// Official list can be found here https://codex.wordpress.org/Embeds
		$video_sites = array(
			'youtube', // first for performance
			'collegehumor',
			'dailymotion',
			'funnyordie',
			'ted',
			'videopress',
			'vimeo',
		);
		$is_video = false;
		// Determine if embed is a video
		foreach ( $video_sites as $site ) {
			// Match on `$html` instead of `$url` because of
			// shortened URLs like `youtu.be` will be missed
			if ( strpos( $html, $site ) ) {
				$is_video = true;
				break;
			}
		}
		// Process video embed
		if ( true == $is_video ) {
			// Find the `<iframe>`

			$class = 'responsive-embed widescreen'; // Foundation class
				// Wrap oEmbed markup in Foundation responsive embed
			return '<div class="' . $class . '">' . $html . '</div>';
		} else { // not a supported embed
			return $html;
		}
	}
	add_filter( 'embed_oembed_html', 'newspapers_responsive_video_oembed_html', 10, 4 );
endif;
