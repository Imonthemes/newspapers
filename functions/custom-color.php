<?php
/**
 * newspapers Theme Custom color
 *
 * @package themezwp
 * @subpackage newspapers
 * @since newspapers 1.0.0
 */

function newspapers_inline_style() {
	$inline_css='';
	// Get the background color for text
$newspapers_flavor_color   =  get_theme_mod( 'newspapers_flavor_color' ,'#00bcd4') ;

// TODO: box_shadow test
if ( 225 > ariColor::newColor( $newspapers_flavor_color )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$text_color = Kirki_Color::adjust_brightness( $newspapers_flavor_color, 500 );
} else {

	// Our background color is light, so we need to create a dark text color
	$text_color = Kirki_Color::adjust_brightness( $newspapers_flavor_color, -225 );

}
/*  Color calculation for text */
$inline_css .=
	".tagcloud a ,
	.post-cat-info a,
	.lates-post-warp .button.secondary,
	.comment-form .form-submit input#submit,
	a.box-comment-btn,
	.comment-form .form-submit input[type='submit'],
	h2.comment-reply-title,
	.widget_search .search-submit,
	.woocommerce nav.woocommerce-pagination ul li span.current,
	.woocommerce ul.products li.product .button,
	.woocommerce div.product form.cart .button,
	.woocommerce #respond input#submit.alt, .woocommerce a.button.alt,
	.woocommerce button.button.alt, .woocommerce input.button.alt,
	.woocommerce #respond input#submit, .woocommerce a.button,
	.woocommerce button.button, .woocommerce input.button,
	.pagination li a,
	.author-links a
	{
		color: $text_color !important;
	}"
;
// Get the background color for text hover
$newspapers_hover_color =  get_theme_mod( 'newspapers_hover_color','#2f2f2f' );

if ( 225 > ariColor::newColor( $newspapers_hover_color )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$hover_text_color = Kirki_Color::adjust_brightness( $newspapers_hover_color, 500 );

} else {
	// Our background color is light, so we need to create a dark text color
	$hover_text_color = Kirki_Color::adjust_brightness( $newspapers_hover_color, -225 );

}

	/* Color calculation for text */
	$inline_css .=
		".tagcloud a:hover ,
		.post-cat-info a:hover,
		.lates-post-warp .button.secondary:hover,
		.comment-form .form-submit input#submit:hover,
		a.box-comment-btn:hover,
		.comment-form .form-submit input[type='submit']:hover,
		.widget_search .search-submit:hover,
		.pagination li a:hover,
		.author-links a:hover
		{
			color: $hover_text_color !important;
		}"
	;
	/*----------- Get the background color for slider cat -----------*/

	$default = array(
		'text'    => '#fff',
		'catbg'   => '#A683F5',
	);
	$value      = get_theme_mod( 'slidertext_color', $default );
	// Sanitize values and convert to valid HEX values.
	$catbg_hex   = ariColor::newColor( $value['catbg'] )->toCSS( 'hex' );

	if ( 225 > ariColor::newColor( $catbg_hex )->luminance )  {

	// Our background color is dark, so we need to create a light text color.
	$catbg_hex = Kirki_Color::adjust_brightness( $catbg_hex, 225 );

} else {

	// Our background color is light, so we need to create a dark text color
	$catbg_hex = Kirki_Color::adjust_brightness( $catbg_hex, - 255 );

	}

	$inline_css .=
		".slider-container .cat-info-el,
		.slider-right .post-header .post-cat-info .cat-info-el
		{
			color: $catbg_hex !important;
		}"
	;

	// Get the background color for text
	$secondary_bgcolor = get_theme_mod( 'secondary_bgcolor' );

	if ( 125 > ariColor::newColor( $secondary_bgcolor )->luminance )  {

	// Our background color is dark, so we need to create a light text color.
	$body_text_color = Kirki_Color::adjust_brightness( $secondary_bgcolor, 200 );

	} else {

	// Our background color is light, so we need to create a dark text color
	$body_text_color = Kirki_Color::adjust_brightness( $secondary_bgcolor, - 220 );

	}
	$inline_css .=
		"
		woocommerce-product-details__short-description,
		.woocommerce div.product .product_title,
		.woocommerce div.product p.price,
		.woocommerce div.product span.price
		{
			color: $body_text_color ;
		}"
	;
	// Get the hover color for text
	$newspapers_hover_color = get_theme_mod( 'newspapers_hover_color' );

	if ( 125 > ariColor::newColor( $newspapers_hover_color )->luminance )  {

	// Our background color is dark, so we need to create a light text color.
	$adjushover_text_color = Kirki_Color::adjust_brightness( $newspapers_hover_color, 200 );

	} else {

	// Our background color is light, so we need to create a dark text color
	$adjushover_text_color = Kirki_Color::adjust_brightness( $newspapers_hover_color, - 220 );

	}
	$inline_css .=
		".woocommerce div.product div.summary a
		{
			color: $adjushover_text_color ;
		}"
	;
	if ( true == get_theme_mod( 'newspapers_body_fullwidth', false ) ) :
		$inline_css .=
			".single-content-wrap,
			.single-post-header
			{
				box-shadow: 0 1px 3px 0 rgba(28, 28, 28, .05);
				-wekit-box-shadow: 0 1px 3px 0 rgba(28, 28, 28, .05);
			}"
		;
		endif;

// Footer widgets area
// Get the background color
$newspapers_footerwidbg_color =  get_theme_mod( 'footerwid_bg_color','#282828' );

if ( 225 > ariColor::newColor( $newspapers_footerwidbg_color )->luminance ) {
	// Our background color is dark, so we need to create a light text color.
	$footerwid_text_color = Kirki_Color::adjust_brightness( $newspapers_footerwidbg_color, 225 );

} else {
	// Our background color is light, so we need to create a dark text color
	$footerwid_text_color = Kirki_Color::adjust_brightness( $newspapers_footerwidbg_color, -225 );

}

	/* Color calculation for text */
	$inline_css .=
		"#footer .top-footer-wrap .textwidget p,
		#footer .top-footer-wrap,
		#footer .block-content-recent .card-section .post-list .post-title a,
		#footer .block-content-recent .post-list .post-meta-info .meta-info-el,
		#footer .widget_nav_menu .widget li a,
		#footer .widget li a
		{
			color: $footerwid_text_color  ;
		}"
	;

wp_add_inline_style( 'newspapers-style', $inline_css );
}
add_action( 'wp_enqueue_scripts', 'newspapers_inline_style', 10 );
