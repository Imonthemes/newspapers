jQuery(window).bind('load', function(){
//REPLACE DUMMY CONTENT BUTTON FUNCTIONALITY
wp.customize.previewer.bind( 'focus-frontsidebar', function(){
	wp.customize.section( 'sidebar-widgets-sidebar-homepagewidgets' ).focus();
	});

	wp.customize.previewer.bind( 'focus-homesidebar', function(){
		wp.customize.section( 'sidebar-widgets-sidebar-homepagesidebar' ).focus();
		});
});

jQuery(document).ready(function($) {

	    //Scroll to panel
	    $('body').on('click', '#accordion-panel-newspapersfooter_options  .accordion-section-title', function(event) {
	        var section_id = $(this).parent('.control-section').attr('id');
	        scrollTopanel( section_id );
	    });
			$('body').on('click', '#accordion-panel-header_options  .accordion-section-title', function(event) {
					var section_id = $(this).parent('.control-section').attr('id');
					scrollTopanelheader( section_id );
			});
			//Scroll to section
	    $('body').on('click', '#sub-accordion-panel-homepage_options .control-subsection .accordion-section-title', function(event) {
	        var section_id = $(this).parent('.control-subsection').attr('id');
	        scrollToSection( section_id );
	    });
	});
	function scrollTopanel( section_id ){
	    var preview_section_id = "footer";

	    var $contents = jQuery('#customize-preview iframe').contents();

	    switch ( section_id ) {
	        case 'accordion-panel-newspapersfooter_options':
	        preview_section_id = "footer";
	        break;

	    }

	    if( $contents.find('#'+preview_section_id).length > 0 ){
	        $contents.find("html, body").animate({
	        scrollTop: $contents.find( "#" + preview_section_id ).offset().top -50
	        }, 1000);
	    }
	}
	function scrollTopanelheader( section_id ){
			var preview_section_id = "header-top";

			var $contents = jQuery('#customize-preview iframe').contents();

			switch ( section_id ) {
					case 'accordion-panel-header_options':
					preview_section_id = "header-top";
					break;

			}

			if( $contents.find('#'+preview_section_id).length > 0 ){
					$contents.find("html, body").animate({
					scrollTop: $contents.find( "#" + preview_section_id ).offset().top - 50
					}, 1000);
			}
	}
	function scrollToSection( section_id ){
	    var preview_section_id = "top-content";

	    var $contents = jQuery('#customize-preview iframe').contents();

	    switch ( section_id ) {
	        case 'accordion-section-newspapers_homepage_slidersettings':
	        preview_section_id = "top-content";
	        break;
					case 'accordion-section-sidebar-widgets-sidebar-homepagewidgets':
				 preview_section_id = "news-latest-post";
				 break;
				 case 'accordion-section-sidebar-widgets-sidebar-homepagesidebar':
					preview_section_id = "news-latest-post";
					break; 

	    }

	    if( $contents.find('#'+preview_section_id).length > 0 ){
	        $contents.find("html, body").animate({
	        scrollTop: $contents.find( "#" + preview_section_id ).offset().top -40
	        }, 1000);
	    }
	}
