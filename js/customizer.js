/**
 * Handles the customizer live preview settings.
 */
( function( $ ) {

		var api = wp.customize;

		// Site title and description.
		api( 'blogname', function( value ) {
			value.bind( function( to ) {
				$( '.site-title a' ).text( to );
			} );
		} );
		api( 'blogdescription', function( value ) {
			value.bind( function( to ) {
				$( '.site-description' ).text( to );
			} );
		} );


	// add class  body.
	api( 'footerwid_row_control', function( value ) {
		value.bind( function( to ) {
			$( '#footer' ).find( '.sidebar-footer' ).

			removeClass('large-12 large-6 large-4 large-3' ).
			addClass(to );
		} );
	} );
	// add class  main menu.
	api( 'newspapers_mainmenu_position', function( value ) {
		value.bind( function( to ) {
			$( '#header-bottom' ).find( '.menu-position' ).

			removeClass('top-bar-left float-center top-bar-right' ).
			addClass(to );
		} );
	} );

} )( jQuery ); // jQuery( document ).ready

jQuery(window).ready(function() {
//Replace Button
jQuery('.add_widget_home').on( 'click', function ( e ) {
	e.preventDefault();
	wp.customize.preview.send( 'focus-frontsidebar');
});
//Replace Button
jQuery('.add_widget_homeside').on( 'click', function ( e ) {
	e.preventDefault();
	wp.customize.preview.send( 'focus-homesidebar');
});

});
