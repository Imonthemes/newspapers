/*=============================================>>>>>
= FUNCTION for before js load =
===============================================>>>>>*/

(function($) {
    'use strict';

    function PagePreloader() {
      jQuery('body').removeClass('no-js');
    }
    jQuery(window).load(function($) {
      PagePreloader();
    });
  }

)(window.jQuery);

/* --------------------------------------------
MAIN FUNCTION
-------------------------------------------- */

jQuery(document).ready(function($) {
  //call foundation 6
  $(document).foundation();

  /*----------- flexslider for featured slider -----------*/

  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
  $(window).bind('#slider', function() {
    setTimeout(function() {
      var slider = $('.flexslider').data('flexslider');
      slider.resize();
    }, 500);
  });


  // Add css class
  $('.single-nav .nav-links').addClass('grid-x');
  $('.single-nav .nav-links .nav-previous').addClass('cell large-6 small-12 float-left nav-left');
  $('.single-nav .nav-links .nav-next').addClass('cell large-6 small-12 float-right nav-right');
  $('.flex-control-thumbs li').addClass('image');
  $('.flex-control-thumbs li img').addClass('object-fit_scale-down');
  $('.woocommerce-pagination ul.page-numbers').addClass('pagination');



  // animationfor menu dropdown
  $('.head-bottom-area .dropdown.menu>li.opens-right').hover(function() {
    $('.head-bottom-area .dropdown.menu>li.opens-right>.is-dropdown-submenu>li.is-dropdown-submenu-item').addClass('animated fadeInUp');
  });

  // animation for stiky menu
  $('.head-bottom-area').on('sticky.zf.stuckto:top', function() {
    $(this).addClass(' fadeInDown');
  }).on('sticky.zf.unstuckfrom:top', function() {
    $(this).removeClass(' fadeInDown');
  })

  // ---------------------------------------------------------------
  // SlideUpTopBar for Foundation top-bar
  // ---------------------------------------------------------------

  var $window = $(window);
  var didScroll;
  var lastScrollTop = 20;
  var scrollAmount = 10; // Value of scroll amount
  var navbarHeight = $('.mobile-menu .sticky.is-stuck').outerHeight();

  $(window).scroll(function(event) {
    didScroll = true;
  });

  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 250);

  function hasScrolled() {

    "use strict";

    var sup = $(window).scrollTop();

    if (Math.abs(lastScrollTop - sup) <= scrollAmount) return;

    if (sup > lastScrollTop && sup > navbarHeight) {
      // On Scroll Down
      $('.mobile-menu .sticky.is-stuck').css({
        top: -$(window).outerHeight()
      });
    } else {
      // On Scroll Up
      if (sup + $(window).height() < $(document).height()) {
        $('.mobile-menu .sticky.is-stuck').css({
          top: 0
        });
      }
    }
    lastScrollTop = sup;
  }



     // Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
     $(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr('rel','gallery').fancybox();

});
/*= End of Main JS =*/
/*=============================================<<<<<*/

/*----------- search Bar animation -----------*/

jQuery(document).ready(function($) {
  var $wrap = $('[open-search]');
  var $close = $('[close-search]');
  $close.on('click', function() {
    $wrap.toggleClass('open');
  });
  $("#btnStats").click($("#dvStats").css("display", "block"));
});
