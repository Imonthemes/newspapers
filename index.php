<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package newspapers
 */

get_header(); ?>
<!--Call Sub Header-->
<?php if ( ( true == get_theme_mod( 'on_of_postpagesubhead', true ) ) ) :?>

<?php if ( is_front_page() && is_home() ) {
  // Notitle Default homepage
} elseif ( is_home() ) {
echo subheader_post();
}?>
<?php endif; ?>
<!--Call Sub Header-->
  <div id="blog-content">
    <div class="grid-container">
      <div class="grid-x grid-padding-x align-center <?php if ( !is_active_sidebar( 'right-sidebar' ) ){ ?> no-left-padding  <?php }?>">
        <div class="cell  small-12 margin-vertical-1<?php if ( !is_active_sidebar( 'right-sidebar' ) ) : ?> large-10 medium-11 <?php else : ?> large-8  <?php endif;?>">
          <div class="lates-post-blog lates-post-blogbig <?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?> margin-horizontal <?php endif;?> "  >

            <?php if ( have_posts() ) : ?>

              <?php /* Start the Loop */ ?>
              <?php while ( have_posts() ) : the_post(); ?>
                <?php
                /*
                * Include the Post-Format-specific template for the content.
                * If newspapers want to override this in a child theme, then include a file
                * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                */
                get_template_part( 'parts/content', get_post_format() );
                ?>

              <?php endwhile; ?>

              <?php get_template_part('pagination'); ?>

            <?php else : ?>

              <?php get_template_part( 'parts/content', 'none' ); ?>

            <?php endif; ?>
          </div><!--POST END-->
        </div>

      <?php get_template_part('sidebar'); ?>
        <!--sidebar END-->
      </div>
    </div>
  </div><!--container END-->


<?php get_footer(); ?>
