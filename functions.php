<?php

/**
* newspapers functions and definitions
*
* For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
*/

global $post;
/**
* Set the content width in pixels, based on the theme's design and stylesheet.
*
* Priority 0 to make it available to lower priority callbacks.
*
* @global int $content_width
*/
function newspapers_content_width()
{
    $GLOBALS['content_width'] = apply_filters('newspapers_content_width', 1000);
}
add_action('after_setup_theme', 'newspapers_content_width', 0);



if (! function_exists('newspapers_setup')) :
//**************newspapers SETUP******************//
function newspapers_setup()
{

    //Register Menus
    register_nav_menus(array(
            'primary' => __('Primary Navigation(Header)', 'newspapers'),
        'top-bar'  	=> esc_html__('Top bar menu', 'newspapers')
        ));
    /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
    add_theme_support('title-tag');


    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');


    // Declare WooCommerce support
    add_theme_support('woocommerce');

    //Custom Background
    add_theme_support('custom-background', array(
'default-color' => 'f7f7f7',

));

    /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
    add_theme_support('html5', array(
'search-form',
'comment-form',
'comment-list',
'gallery',
'caption',
));

    /*
    * Enable support for custom Header image.
    *
    *  @since newspapers
    */
    $args = array(
'flex-width'    => true,
'flex-height'   => true,
'default-image' => get_template_directory_uri() . '/images/header.jpg',
'header-text'            => false,
);
    add_theme_support('custom-header', $args);

    //Post Thumbnail
    add_theme_support('post-thumbnails');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
    /*

    /*
    * Enable support for custom logo.
    *
    *  @since newspapers
    */

    $defaults = array(
'height'      => 80,
'width'      => 180,
'flex-width'  => true,
'header-text' => array( 'site-title', 'site-description' ),
);
    add_theme_support('custom-logo', $defaults);

    /*
    * Enable support for Post Thumbnails on posts and pages.
    *
    * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    */
    add_theme_support('post-thumbnails');

    // Add featured image sizes
//
// Sizes are optimized and cropped for landscape aspect ratio
// and optimized for HiDPI displays on 'small' and 'medium' screen sizes.
add_image_size('newspapers-small', 735, 400, true); // name, width, height, crop
add_image_size('newspapers-medium', 428, 400, true);
    add_image_size('newspapers-large', 735, 400, true);
    add_image_size('newspapers-xlarge', 1920, 400, true);
    add_image_size('newspaperstop-medium', 540, 400, true);
    add_image_size('newspapers-listpost-small', 110, 85, true);
    add_image_size('newspapers-xxlarge', 1920, 600, true);


    // Add theme support for woocommerce product gallery added in WooCommerce 3.0.
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-slider');

    /*
    * Make theme available for translation.
    * Translations can be filed in the /languages/ directory.
    * If newspapers're building a theme based on newspapers, use a find and replace
    * to change 'newspapers' to the name of newspapersr theme in all the template files
    */

    load_theme_textdomain('newspapers', get_template_directory() . '/languages');

    add_theme_support('starter-content', array(

'posts' => array(
'home',
'blog' ,
),

'options' => array(
'show_on_front' => 'page',
'page_on_front' => '{{home}}',
'page_for_posts' => '{{blog}}',
),


'nav_menus' => array(
'primary' => array(
'name' => __('Primary Navigation(Header)', 'newspapers'),
'items' => array(
'page_home',
'page_about',
'page_blog',
'page_contact',
),
),
),
));
}
endif; // newspapers_setup
add_action('after_setup_theme', 'newspapers_setup');


/**
* The CORE functions for newspapers
*
* Stores all the core functions of the template.
*
* @package newspapers
*
* @since newspapers 1.0
*/


if (! function_exists('newspapers_the_custom_logo')) :
/**
* Displays the optional custom logo.
*
* Does nothing if the custom logo is not available.
*
* @since newspapers
*/
function newspapers_the_custom_logo()
{
    if (function_exists('the_custom_logo')) {
        the_custom_logo();
    }
}
endif;

/**
* Filter the except length to 20 characters.
*
* @param int $length Excerpt length.
* @return int (Maybe) modified excerpt length.
*/
function newspapers_custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'newspapers_custom_excerpt_length', 999);


/**
* Filter the excerpt "read more" string.
*
* @param string $more "Read more" excerpt string.
* @return string (Maybe) modified "read more" excerpt string.
*/
function newspapers_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'newspapers_excerpt_more');

/**
 * Checks whether woocommerce is active or not
 *
 * @return boolean
 */
function newspapers_is_woocommerce_active()
{
    if (class_exists('woocommerce')) {
        return true;
    } else {
        return false;
    }
}
//Load CSS files

function newspapers_scripts()
{
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/fonts/awesome/css/font-awesome.min.css', 'font_awesome', true);
    wp_enqueue_style('newspapers_core', get_template_directory_uri() . '/css/newspapers.min.css', 'newspaperscore_css', true);
    wp_enqueue_style('newspapers-fonts', newspapers_fonts_url(), array(), null);
    wp_enqueue_style('newspapers-style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'newspapers_scripts');


/**
* Google Fonts
*/

function newspapers_fonts_url()
{
    $fonts_url = '';

    /* Translators: If there are characters in newspapersr language that are not
    * supported by Lato, translate this to 'off'. Do not translate
    * into newspapersr own language.
    */
    $lato = _x('on', 'Lato font: on or off', 'newspapers');

    /* Translators: If there are characters in your language that are not
    * supported by Ubuntu, translate this to 'off'. Do not translate
    * into your own language.
    */
    $ubuntu = _x('on', 'Ubuntu font: on or off', 'newspapers');



    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $open_sans = _x('on', 'Open Sans font: on or off', 'newspapers');

    if ('off' !== $lato || 'off' !== $ubuntu || 'off' !== newspapers) {
        $font_families = array();

        if ('off' !== $ubuntu) {
            $font_families[] = 'Ubuntu:400,500,700';
        }

        if ('off' !== $lato) {
            $font_families[] = 'Lato:400,700,400italic,700italic';
        }

        if ('off' !== $open_sans) {
            $font_families[] = 'Open Sans:400,400italic,700';
        }

        $query_args = array(
'family' => urlencode(implode('|', $font_families)),
'subset' => urlencode('latin,latin-ext'),
);

        $fonts_url = add_query_arg($query_args, '//fonts.googleapis.com/css');
    }

    return $fonts_url;
}

//Load Java Scripts
function newspapers_head_js()
{
    if (!is_admin()) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('newspapers_js', get_template_directory_uri().'/js/newspapers.min.js', array('jquery'), true);
        wp_enqueue_script('newspapers_other', get_template_directory_uri().'/js/newspapers_other.min.js', array('jquery'), true);
        if (is_singular()) {
            wp_enqueue_script('comment-reply');
        }
    }
}
add_action('wp_enqueue_scripts', 'newspapers_head_js');

/**
* Register widget area.
*
* @link http://codex.wordpress.org/Function_Reference/register_sidebar
*/
function newspapers_widgets_init()
{
    register_sidebar(array(
'name'          => __('Right Sidebar', 'newspapers'),
'id'            => 'right-sidebar',
'description'   => __('Right Sidebar', 'newspapers'),
'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-item cell small-12 medium-6 large-12"><div class="widget_wrap ">',
'after_widget'  => '</div></div>',
'before_title'  => '<div class="widget-title "> <h3>',
'after_title'   => '</h3></div>'
));
    $footerwid_row_control = get_theme_mod('footerwid_row_control', 'large-4');
    register_sidebar(array(
'name'          => __('Footer Widgets', 'newspapers'),
'id'            => 'foot_sidebar',
'description'   => __('Widget Area for the Footer', 'newspapers'),
'before_widget' => '<div id="%1$s" class="widget %2$s sidebar-footer cell small-12 medium-6 '.$footerwid_row_control.' align-self-top " ><aside id="%1$s" class="widget %2$s">',
'after_widget'  => '</aside></div>',
'before_title'  => '<div class="widget-title "> <h3>',
'after_title'   => '</h3></div>'
));
    register_sidebar(array(
'name'          => __('Header advertising area ', 'newspapers'),
'id'            => 'sidebar-headeradvertising',
'before_widget' => '<div id="%1$s" class="widget %2$s" data-widget-id="%1$s">',
'after_widget'  => '</div>',
'before_title'  => '<h3 class="widget-title hide">',
'after_title'   => '</h3>'
));
    register_sidebar(array(
'name'          => __('Home page widgets area', 'newspapers'),
'id'            => 'sidebar-homepagewidgets',
'before_widget' => '<div id="%1$s" class="widget %2$s " data-widget-id="%1$s">',
'after_widget'  => '</div>',
'before_title'  => '<h3 class="widget-title">',
'after_title'   => '</h3>'
));
    register_sidebar(array(
'name'          => __('Home page sidebar', 'newspapers'),
'id'            => 'sidebar-homepagesidebar',
'before_widget' => '<div id="%1$s" class="widget %2$s cell small-12 medium-6 large-12 " data-widget-id="%1$s">',
'after_widget'  => '</div>',
'before_title'  => '<div class="widget-title "> <h3>',
'after_title'   => '</h3></div>'
));
}

add_action('widgets_init', 'newspapers_widgets_init');




/**  Pagination founctions */
require_once(get_template_directory() . '/functions/post-navi.php');

/** Configure responsive image sizes */
require_once(get_template_directory() . '/functions/hooks.php');

/** Register all navigation menus */
require_once(get_template_directory() . '/functions/menu.php');

/** founctions for color calculation */
require_once(get_template_directory() . '/functions/custom-color.php');

/**  woocommerce founctions */
if (newspapers_is_woocommerce_active()) {
    require_once(get_template_directory() . '/functions/woo-hooks.php');
}

/** call widgets */
require_once(get_template_directory() . '/inc/widgets/latest-posts-single.php');
require_once(get_template_directory() . '/inc/widgets/latest-post-blog.php');
require_once(get_template_directory() . '/inc/widgets/latest-post-blogbig.php');
require_once(get_template_directory() . '/inc/widgets/recent-posts-single.php');


//load widgets ,kirki ,customizer,functions
require_once(get_template_directory() . '/inc/kirki/kirki.php');
require_once(get_template_directory() . '/inc/customizer.php');
require_once(get_template_directory() . '/inc/welcome/welcome-screen.php');
require_once(get_template_directory() . '/inc/tgm-plugin.php');
/**
 * Implement demo import.
 */
require_once(get_template_directory() . '/functions/function-demo.php');
