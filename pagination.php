<!-- pagination -->
		<?php
			if(function_exists('newspapers_post_navi')) :
				 newspapers_post_navi();
			else :
		?>
			<div class="newspapers_nav">
			<?php	// Previous/next page navigation.
			the_posts_pagination(  array('prev_text' => '&laquo;', 'next_text' => '&raquo;') );?>

			</div>
		<?php endif; ?>
<!-- /pagination -->
