=== Newspapers ===
 
 Contributors: Themezwp
 Tags: featured-images,front-page-post-form, sticky-post, threaded-comments,blog,news,food-and-drink
 Requires at least: 4.0
 Tested up to: 4.9.4
Stable tag: 1.5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
 
 
 = =   F r e q u e n t l y   A s k e d   Q u e s t i o n s   = = 
 
 
 
 =   H o w   d o   I   S e t   U p   s e t u p   s t a t i c   i m a g e ?   = 
 
 
 
 
 
 1 .   g o   t o   p o s t   = >   a d d   n e w   = >   a d d   y o u r   p o s t   t i t l e   a n d   d e s c r i p t i o n   = >   P u b l i s h 
 
 
 
 2 . I f   y o u   d o n t   w a n t   t o   s h o w   t h i s   p o s t   i n   l a t e s t   p o s t   s e c t i o n   . t h e n   c r a t e   a   n e w   c a t e g o r y   a n d   " s t a t i c - i m a g e "   . P u t   t h i s   p o s t   i n t o   t h i s   c a t e g o r y 
 
 
 
 3 .   G o   t o   c u s t o m i z e   = >   S l i d e r   s e t u p   = >   c h o s e   p o s t   f o r   s l i d e r 
 
 
 
 4 .   U p l o a d   s t a t i c   s l i d e r   i m a g e 
 
 
 
 == Changelog ==
= 1.5.0 =

# fixed error when off post header
# fixed slider color option
# added more demo

= 1.4.0 =

# Fixed Post featured image
# Add demo import options

 = 1.0.0 =

= 1.0.8 =

Fix woocommerce issue
Fix Side bar issues

= 1.0.9 =
Fixed sticky post issue
Fixed Responsive menu overlap

= 1.1.0 =
add slider content background colour

= 1.2.0 =
# fixed list style widgets error
= 1.2.0 =
# fixed videos area is overlapping
# fixed AMP error
# fixed sidebar style

== Resources ==

 I m a g e s   C r e a t e d 
 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 

License: CC0 licenses

1) https://www.pexels.com/photo/adventure-alps-backpack-backpacking-633276/
2)https://www.pexels.com/photo/blur-car-cellphone-contemporary-230554/
3)https://static.pexels.com/photos/295208/pexels-photo-295208.jpeg
4)https://www.pexels.com/photo/blur-bottle-bread-bun-287354/
5)https://static.pexels.com/photos/196652/pexels-photo-196652.jpeg
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 C S S   C r e d i t s   
 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 F o n t   A w e s o m e : 
 
 F o n t   L i c e n s e 
 
 h t t p : / / f o n t a w e s o m e . i o / l i c e n s e / 
 
 L i c e n s e :   S I L   O F L   1 . 1 
 
 C o d e   L i c e n s e 
 
 L i c e n s e :   M I T   L i c e n s e 
 
 U R L :   h t t p : / / f o n t a w e s o m e . i o / l i c e n s e / 
  
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 J S   C r e d i t s   
 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
jQuery FlexSlider v2.6.4
Copyright 2012 WooThemes
Contributing Author: Tyler Smith
GNU General Public License v2.0
https://github.com/woocommerce/FlexSlider/blob/master/LICENSE.md

fancyBox - jQuery Plugin
version: 2.1.7 (Tue, 28 Feb 2017)
License: www.fancyapps.com/fancybox/#license
Creative Commons Attribution-NonCommercial 3.0 ( GPLv3 )
Copyright 2017 fancyapps.com

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 f r a m e w o r k   C r e d i t s   
 
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
 K i r k i   T o o l k i t 
 
 h t t p : / / k i r k i . o r g 
 
 l i c e n s e :   h t t p : / / o p e n s o u r c e . o r g / l i c e n s e s / g p l - 2 . 0 . p h p   G N U   P u b l i c   L i c e n s e 
 
 
 
  fo u n d a t i o n   f o r   S i t e s   b y   Z U R B 
 
 f o u n d a t i o n . c s s 
f o u n d a t i o n . js
 
 f o u n d a t i o n . z u r b . c o m 
 
 L i c e n s e d   u n d e r   t h e   M I T   l i c e n s e   -   h t t p : / / o p e n s o u r c e . o r g / l i c e n s e s / M I T 
 
 
 
 
 
 
 
 
 
 
 
 
 
