

<div class="head-top-area " <?php if( !empty($image_heade2) ):?>style="background:url(<?php echo esc_url($image_heade2) ;?>);background-image:cover; background-repeat:no-repeat;" <?php endif ;?>>
  <div class="grid-container ">
    <div class="grid-x grid-padding-x grid-margin-y align-center-middle ">
      <!--  Logo -->
      <?php $logo_position = get_theme_mod( 'logo_position', 'left' );?>
      <div class="cell <?php if ('center' == $logo_position ) : ?> large-12 logo-center<?php else : ?> auto <?php echo newspapers_logo_position(); ?>  <?php endif;?>">
          <div id="site-title" >
            <?php if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) : ?>
              <?php newspapers_the_custom_logo(); ?>
            <?php else : ?>
              <h1 class="site-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
              </h1>
              <?php
                $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                  <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                <?php endif; ?>
            <?php endif;?>
          </div>
        </div>
      <!-- /.End Of Logo -->
      <?php if ( is_active_sidebar( 'sidebar-headeradvertising' ) ) :?>
      <div class="cell <?php if ('center' == $logo_position ) : ?> large-12 <?php else : ?> large-8   <?php endif;?>">
       <div class="<?php if ('center' == $logo_position ) : ?> logo-center <?php else : ?> float-right <?php endif;?>">
          <?php dynamic_sidebar( 'sidebar-headeradvertising' );?>
       </div>
      </div>
      <?php endif;?>
    </div>
  </div>
</div>
