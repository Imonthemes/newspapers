<?php
/**
 * The TOP header
 */
?>
<?php if ( (has_nav_menu( 'top-bar' ) ) || ( get_theme_mod( 'social_icons_top') ) ): ?>
<div id="topmenu"   >
  <div  class="grid-container">
    <div class="top-bar">
      <div class="top-bar-left">
          <?php newspapers_top_bar(); ?>
      </div>
      <div class="top-bar-right">
        <?php $social_icons_top = get_theme_mod( 'social_icons_top'); ?>
        <?php if( !empty( $social_icons_top ) ):?>
        <div class="social-btns">
          <?php foreach( $social_icons_top as $row ) : ?>
            <a class="btn <?php echo esc_html( $row['social_icon']); ?>" <?php if ( true == get_theme_mod( 'open_social_tab', false ) ) : ?>target="_blank"<?php endif; ?> href="<?php echo esc_url($row['social_url']); ?>">
              <i class="fa fa-<?php echo esc_html( $row['social_icon']); ?>"></i>
            </a>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
</div>
<?php endif; ?>
