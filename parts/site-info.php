<?php
/**
 * Displays footer site info
 *
 * @package Themez Wp
 * @subpackage newspapers
 * @since 1.0
 * @version 1.0
 */
?>

  <div class="footer-copyright-text"> 
  <?php
     $newspapers_footertext = get_theme_mod ('news_copyright_text');
     echo $newspapers_footertext;
 ?>
</div>
