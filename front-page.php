<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Newspapers
 * @since Newspapers 1.0
 */
get_header();

    echo '<div id="top-content">';/* satrt top section */
     if ( true == get_theme_mod( 'newspapers_body_fullwidth', false ) ) :
      echo '<div class="grid-container fluid">';
      else :
        echo '<div class="grid-container">';
      endif;
        echo  '<div class=" grid-x grid-margin-x align-center" >';
          get_template_part('home-part/part', 'slider');
          get_template_part('home-part/part', 'rightpost');
        echo '</div>';
      echo '</div>';
    echo '</div>'; /* End top section */
    get_template_part('home-part/latest', 'post');

get_footer();
