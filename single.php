
<?php get_header();
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage newspapers
 * @since newspapers 1.0
 */

 ?>


  <?php get_template_part( 'parts/content', 'single' ); ?>


<?php get_footer(); ?>
